package com.victor.parra.view.csv;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.AbstractView;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import com.victor.parra.models.entities.Cliente;

@Component("listar.csv")
public class ClienteCsvViel extends AbstractView {
	
	public ClienteCsvViel() {
		setContentType("txt/csv");
	}

	@Override
	protected boolean generatesDownloadContent() {
		// TODO Auto-generated method stub
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void renderMergedOutputModel(Map<String, Object> model, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		response.setHeader("Content-Disposition", "attachment; filename=\"clientes.csv");
		response.setContentType(getContentType());
		
		model.get("clientes");
		
		
		Page<Cliente> clientes = (Page<Cliente>) model.get("clientes");
		
		ICsvBeanWriter beanWriter = new CsvBeanWriter(response.getWriter(), CsvPreference.STANDARD_PREFERENCE);
		
		String[] header = {"id", "nombre", "apellido", "dni", "correo", "telefono", "createAt"};
		beanWriter.writeHeader(header);	
	for(Cliente cliente: clientes) {
		beanWriter.write(cliente,header);
	}
	
	beanWriter.close();
	}

}
