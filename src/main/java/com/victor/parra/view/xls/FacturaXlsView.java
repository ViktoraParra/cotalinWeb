package com.victor.parra.view.xls;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.document.AbstractXlsxView;

import com.victor.parra.models.entities.Factura;
import com.victor.parra.models.entities.ItemFactura;

@Component("factura/ver.xlsx")
public class FacturaXlsView extends AbstractXlsxView{

	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		// se debe llamar igual que lo tenemos en el controlador
		response.setHeader("Content-Disposition", "attachment; filename=\"factura_view.xlsx\"");
		Factura factura = (Factura) model.get("factura");
		Sheet sheet = workbook.createSheet("Factura");
		
		MessageSourceAccessor mensajes = getMessageSourceAccessor();
		
		Row row = sheet.createRow(0);
		Cell cell = row.createCell(0);
		
		cell.setCellValue(mensajes.getMessage("text.factura.ver.datos.cliente"));
		row = sheet.createRow(3);
		cell = row.createCell(0);
		cell.setCellValue(factura.getCliente().getNombre() + " " + factura.getCliente().getApellido());
		
		row = sheet.createRow(4);
		cell = row.createCell(0);
		cell.setCellValue(factura.getCliente().getCorreo());
		
		sheet.createRow(6).createCell(0).setCellValue(mensajes.getMessage("text.factura.ver.datos.factura"));
		sheet.createRow(7).createCell(0).setCellValue(mensajes.getMessage("text.cliente.factura.folio")  + factura.getId());
		sheet.createRow(8).createCell(0).setCellValue(mensajes.getMessage("text.cliente.factura.descripcion") + factura.getDescripcion());
		sheet.createRow(9).createCell(0).setCellValue(mensajes.getMessage("text.cliente.factura.fecha") + factura.getCreateAt());
		
		Row header = sheet.createRow(11);
		header.createCell(0).setCellValue(mensajes.getMessage("text.factura.form.item.nombre"));
		header.createCell(1).setCellValue(mensajes.getMessage("text.factura.form.item.precio"));
		header.createCell(2).setCellValue(mensajes.getMessage("text.factura.form.item.cantidad"));
		header.createCell(3).setCellValue(mensajes.getMessage("text.factura.form.item.total"));
		
		int rownum = 12;
		for(ItemFactura item: factura.getItems()) {
			Row fila = sheet.createRow(rownum ++);
			fila.createCell(0).setCellValue(item.getProducto().getNombre());
			fila.createCell(1).setCellValue(item.getProducto().getPrecio());
			fila.createCell(2).setCellValue(item.getCantidad());
			fila.createCell(3).setCellValue(item.calcularImporte());
		}
		
		Row filatotal = sheet.createRow(rownum);
		filatotal.createCell(2).setCellValue(mensajes.getMessage("text.factura.form.total") + ": ");
		filatotal.createCell(3).setCellValue(factura.getTotal());
		
	}

}
