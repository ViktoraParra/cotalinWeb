package com.victor.parra.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.victor.parra.models.dao.IUsuarioDao;
import com.victor.parra.models.entities.Role;
import com.victor.parra.models.entities.Usuario;

@Service("jpaUserDetailsService")
public class JpaUserDetailsService implements UserDetailsService{

	@Autowired
	private IUsuarioDao usuarioDao;
	
	private Logger logger = LoggerFactory.getLogger(JpaUserDetailsService.class);
	
	// esto indica cargar el usuario por su Username
	@Override
	@Transactional(readOnly = true)
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		// Realizamos la inyeccion de los usuarios
		Usuario usuario = usuarioDao.findByUsername(username);

		// verificamos que el usuario exista en el sistema
				if (usuario ==null) {
					logger.error("Error Login: no existe el usuario '" + username + "'");
					throw new UsernameNotFoundException("Username" + username + "no existe en el sistema!");
				}
				
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		
		//Realizamos la inyeccion de los roles designandoles los roles
		for(Role role: usuario.getRoles()) {
			logger.info("Role: ".concat(role.getAuthority()));
			authorities.add(new SimpleGrantedAuthority(role.getAuthority()));
		}
		
		//verificamos el caso de existir en el sistema posea un rol asignado
		if (authorities.isEmpty()) {
			logger.error("Error Login: usuario '" + username + "' no tiene roles asignado!");
			throw new UsernameNotFoundException("Username" + username + "no tiene roles en el sistema!");
		}
		return new User(usuario.getUsername(), usuario.getPassword(), usuario.getEnable(), true, true, true, authorities);
	}

}
