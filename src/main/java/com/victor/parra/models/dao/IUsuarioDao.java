package com.victor.parra.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.victor.parra.models.entities.Usuario;

public interface IUsuarioDao extends CrudRepository<Usuario, Long>{
	
	public Usuario findByUsername(String username);

}
