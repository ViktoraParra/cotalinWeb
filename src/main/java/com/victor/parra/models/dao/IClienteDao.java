package com.victor.parra.models.dao;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.victor.parra.models.entities.Cliente;

public interface IClienteDao extends PagingAndSortingRepository<Cliente, Long>{
	/*
	@Query("select c from Cliente c where c.email = %?1%")
	Cliente findByEmail(String email);
	
	@Query("select c from Cliente c where c.nombre like %?1%")
	Cliente findByNombreLike(String nombre);
	
	@Query("select c from Cliente c where c.dni like %?1%")
	Cliente findByDniLike(String dni);
	*/
	
	// realiza una consulta query language donde realiza consulta no a la base de datos
	// Sino a la entidad
	@Query("select c from Cliente c left join fetch c.facturas f where c.id=?1")
	public Cliente fetchByIdWithFacturas(Long id);
}
