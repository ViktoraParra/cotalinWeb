package com.victor.parra.controllers;

import java.security.Principal;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class LoginController {
	
	@Autowired
	private MessageSource messageSource;

	// Esta clase se encarga de realizar la validación para el inicio de sesión
	// se determina el mensaje de error
	@GetMapping("/login")
	public String login(@RequestParam(value = "error", required = false) String error,
			@RequestParam(value = "logout", required = false) String logout,
			Model model, Principal principal, RedirectAttributes flash, Locale locale) {
		
		// Realiza la verificacion si ya esta logueado el usuario
		if (principal != null) {
			flash.addFlashAttribute("info", messageSource.getMessage("text.login.already", null, locale));
			return "redirect:/";
		}
		// realizo la verificacion para el inicio de sesion 
		if (error != null) {
			model.addAttribute("error", messageSource.getMessage("text.login.error", null, locale));
		}
		// realizo la verificación que se ha deslogeado con exito
		if (logout != null) {
			model.addAttribute("success", messageSource.getMessage("text.login.logout", null, locale));
		}
		
		return "login";
	}
}
