package com.victor.parra;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import com.victor.parra.auth.handler.loginSuccessHandler;
import com.victor.parra.service.JpaUserDetailsService;

@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
@Configuration
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter{
	
	@Autowired
	private loginSuccessHandler successHandler;
	
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	
//	@Autowired
//	private DataSource dataSource;
	
	@Autowired
	private JpaUserDetailsService userDetailsService;

	/*
	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}*/
	
	// Nos permite realizar la configuracion de acceso a las rutas
	// determinando los permisos que tiene cada rol y los accesos que
	// tendra dentro de la aplicacion y gestionamos los permisos de
	// la pagina login y logout
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		http.authorizeRequests().antMatchers("/", "/css/**", "/js/**", "/images/**", "/listar**", "/locale").permitAll()
		/*.antMatchers("/ver/**").hasAnyRole("USER")
		.antMatchers("/uploads/**").hasAnyRole("USER")
		.antMatchers("/form/**").hasAnyRole("ADMIN")
		.antMatchers("/eliminar/**").hasAnyRole("ADMIN")
		.antMatchers("/factura/**").hasAnyRole("ADMIN")*/
		.anyRequest().authenticated()
		.and()
		.formLogin()
			.successHandler(successHandler)
			.loginPage("/login")
		.permitAll()
		.and()
		.logout().permitAll()
		.and()
		.exceptionHandling().accessDeniedPage("/error_403");
	}

	@Autowired
	public void configurerGlobal(AuthenticationManagerBuilder build) throws Exception{
		
		// Realizamos la conexion y codificacion mediante JPA
		build.userDetailsService(userDetailsService)
		.passwordEncoder(passwordEncoder);
		

//		// realizamos la inyeccion de los usuarios mediante JDBC con consultas nativas SQL
//		build.jdbcAuthentication()
//		.dataSource(dataSource)
//		.passwordEncoder(passwordEncoder)
//		// realizamos la confirmacion de los usuarios registrados en la tabla con sus accesos
//		.usersByUsernameQuery("select username, password, enable from users where username=?")
//		// realizamos la verificacion de los permisos otorgados a los ususarios
//		.authoritiesByUsernameQuery("select u.username, a.authority from authorities a inner join users u on(a.user_id=u.id) where u.username=?");
		
		
//		PasswordEncoder encoder = this.passwordEncoder;
		// Expresion lamda
		/* UserBuilder users = User.builder().passwordEncoder(password -> {
		 * return encoder.encode(password);
		 * opcion 2
		 * UserBuilder users = User.builder().passwordEncoder(password -> encoder.encode(password));
		 *  Opcion mas simplificada
		 * */
		// creo la encriptacion de las constraseñas de los usuarios tengo
	
		
//		UserBuilder users = User.builder().passwordEncoder(encoder::encode);
//		
//		// creo los usuarios y les asigno la contraseña mediante el codigo
//		builder.inMemoryAuthentication()
//		.withUser(users.username("admin").password("12345").roles("ADMIN","USER"))
//		.withUser(users.username("paola").password("12345").roles("USER"));
	}

}
