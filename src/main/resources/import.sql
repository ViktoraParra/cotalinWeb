/* Populate tables */
INSERT INTO clientes(id, nombre, apellido, dni, correo, telefono, foto, create_at) VALUES(1, 'Victor', 'Parra', '34902123', 'victorparra@gmail.com', '1152444523', '', '1985-03-27');
INSERT INTO clientes(id, nombre, apellido, dni, correo, telefono, foto, create_at) VALUES(2, 'Luciana', 'Perozo', '33902123', 'lucianaperozo@gmail.com', '1127044091', '', '1992-08-14');
INSERT INTO clientes(id, nombre, apellido, dni, correo, telefono, foto, create_at) VALUES(3, 'Daniel', 'Jimenez', '30492163', 'danieljimenz@gmail.com', '1123211158', '', '1991-12-27');
INSERT INTO clientes(id, nombre, apellido, dni, correo, telefono, foto, create_at) VALUES(4, 'Carlos', 'Colmenarez', '32731891', 'carloscol@gmail.com', '1164227963', '', '1993-07-19');
INSERT INTO clientes(id, nombre, apellido, dni, correo, telefono, foto, create_at) VALUES(5, 'Ricardo', 'Urrutia', '33001234', 'ricurri@gmail.com', '1169918352', '', '1988-08-30');
INSERT INTO clientes(id, nombre, apellido, dni, correo, telefono, foto, create_at) VALUES(6, 'Ale', 'Chen', '94821621', 'alechen@gmail.com', '1154146006', '', '1992-02-22');
INSERT INTO clientes(id, nombre, apellido, dni, correo, telefono, foto, create_at) VALUES(7, 'Victor', 'Colmenarez', '92341874', 'victorcol@gmail.com', '1125810114', '', '1987-11-02');
INSERT INTO clientes(id, nombre, apellido, dni, correo, telefono, foto, create_at) VALUES(8, 'Juana', 'Avila', '31674385', 'juanaavila@gmail.com', '2945696612', '', '1994-03-07');
INSERT INTO clientes(id, nombre, apellido, dni, correo, telefono, foto, create_at) VALUES(9, 'Romina', 'Fernandez', '32553821', 'romferna@gmail.com', '1164998704', '', '1990-09-09');
INSERT INTO clientes(id, nombre, apellido, dni, correo, telefono, foto, create_at) VALUES(10, 'Jimena', 'Galindo', '35602898', 'jimegalin@gmail.com', '1163580334', '', '1991-04-20');
INSERT INTO clientes(id, nombre, apellido, dni, correo, telefono, foto, create_at) VALUES(11, 'Vanesa', 'Perez', '30990889', 'vanepere@gmail.com', '1158218883', '', '1989-08-17');
INSERT INTO clientes(id, nombre, apellido, dni, correo, telefono, foto, create_at) VALUES(12, 'Camilo', 'Sundinga', '93672810', 'camsundi@gmail.com', '1132705194', '', '1995-10-10');
INSERT INTO clientes(id, nombre, apellido, dni, correo, telefono, foto, create_at) VALUES(13, 'Cristian', 'Colis', '34718295', 'criscolis@gmail.com', '1168403562', '', '1989-04-25');
INSERT INTO clientes(id, nombre, apellido, dni, correo, telefono, foto, create_at) VALUES(14, 'Juan', 'Camats', '91787552', 'juancamats@gmail.com', '1157207890', '', '1990-03-29');
INSERT INTO clientes(id, nombre, apellido, dni, correo, telefono, foto, create_at) VALUES(15, 'Carolina', 'Truvel', '93511909', 'carotruvel@gmail.com', '1168087111', '', '1989-07-13');
INSERT INTO clientes(id, nombre, apellido, dni, correo, telefono, foto, create_at) VALUES(16, 'Pedro', 'Colores', '95038941', 'pedrocolores@gmail.com', '1157720240', '', '1991-08-01');
INSERT INTO clientes(id, nombre, apellido, dni, correo, telefono, foto, create_at) VALUES(17, 'Lucia', 'Barraza', '33249571', 'luciabarra@gmail.com', '1123082341', '', '1985-08-06');
INSERT INTO clientes(id, nombre, apellido, dni, correo, telefono, foto, create_at) VALUES(18, 'Tadeo', 'Topazini', '94671167', 'tatopazini@gmail.com', '1132499196', '', '1992-04-24');
INSERT INTO clientes(id, nombre, apellido, dni, correo, telefono, foto, create_at) VALUES(19, 'Martina', 'Cremona', '93797263', 'marticremo@gmail.com', '1162537394', '', '1985-09-30');
INSERT INTO clientes(id, nombre, apellido, dni, correo, telefono, foto, create_at) VALUES(29, 'Miguel', 'Alares', '32008900', 'miguelalares@gmail.com', '1160499036', '', '1992-11-12');


/* Populate tabla productos */
INSERT INTO productos (nombre, precio, create_at) VALUES('Panasonic Pantalla LCD', 25999, NOW());
INSERT INTO productos (nombre, precio, create_at) VALUES('Sony Camara digital DSC-W320B', 12349, NOW());
INSERT INTO productos (nombre, precio, create_at) VALUES('Apple iPod shuffle', 149999, NOW());
INSERT INTO productos (nombre, precio, create_at) VALUES('Sony Notebook Z110', 3799, NOW());
INSERT INTO productos (nombre, precio, create_at) VALUES('Hewlett Packard Multifuncional F2280', 6999, NOW());
INSERT INTO productos (nombre, precio, create_at) VALUES('Bianchi Bicicleta Aro 26', 6999, NOW());
INSERT INTO productos (nombre, precio, create_at) VALUES('Mica Comoda 5 Cajones', 29999, NOW());
INSERT INTO productos (nombre, precio, create_at) VALUES('Phillip Pantalla LCD', 18320, NOW());
INSERT INTO productos (nombre, precio, create_at) VALUES('Nikon Camara Reflex D5400', 22349, NOW());
INSERT INTO productos (nombre, precio, create_at) VALUES('Xiaomi Redmin Note 7', 19000, NOW());
INSERT INTO productos (nombre, precio, create_at) VALUES('Dell Notebook jask321', 37990, NOW());
INSERT INTO productos (nombre, precio, create_at) VALUES('Samsung S10', 32890, NOW());
INSERT INTO productos (nombre, precio, create_at) VALUES('Nikon D7500', 45390, NOW());
INSERT INTO productos (nombre, precio, create_at) VALUES('Lenovo Notebook T580', 29999, NOW());

/* Creamos algunas facturas */
INSERT INTO facturas (descripcion, observacion, cliente_id, create_at) VALUES('Factura equipos de oficina', null, 1, NOW());
INSERT INTO facturas_items (cantidad, factura_id, producto_id) VALUES(1, 1, 1);
INSERT INTO facturas_items (cantidad, factura_id, producto_id) VALUES(2, 1, 4);
INSERT INTO facturas_items (cantidad, factura_id, producto_id) VALUES(1, 1, 5);
INSERT INTO facturas_items (cantidad, factura_id, producto_id) VALUES(1, 1, 7);

INSERT INTO facturas (descripcion, observacion, cliente_id, create_at) VALUES('Factura Bicicleta', 'Alguna nota importante!', 1, NOW());
INSERT INTO facturas_items (cantidad, factura_id, producto_id) VALUES(3, 2, 6);

-- insertamos los usuarios junto a las contraseñas y sus validacion
INSERT INTO users (username, password, enable) VALUES ('paola','$2a$10$3mBTsKMAyd4apla33iiZqOMV6Dqz9Y.hjBIZBu2ZMqms.xo74fu1G',1);
INSERT INTO users (username, password, enable) VALUES ('admin','$2a$10$6e6WAhprtN0llzw3fOlx4OezsL6L23MxRxlWnUT2hCLKajb3DFAvW',1);
INSERT INTO users (username, password, enable) VALUES ('victor','$2a$10$kGpm9NfvNclILjpaO0Knd.CwbRzb8cvTzYmd5e8PNp86BRkUOkxMi',1);
INSERT INTO users (username, password, enable) VALUES ('darelis','$2a$10$edQHznYqZ9vFGhWPa8abIOUgvFMpvFXg1xGebGzIT0u6PY4Sjf8N.',1);
INSERT INTO users (username, password, enable) VALUES ('sole','$2a$10$DpnAXgbumVPl0QjJl4ni2unAHC5LsoNgZcO2tsshJD3jQB.xGVZHC',1);

-- insertamos los perfiles a los usuarios
INSERT INTO authorities (user_id, authority) VALUES (1, 'ROLE_USER');
INSERT INTO authorities (user_id, authority) VALUES (2, 'ROLE_USER');
INSERT INTO authorities (user_id, authority) VALUES (2, 'ROLE_ADMIN');
INSERT INTO authorities (user_id, authority) VALUES (3, 'ROLE_USER');
INSERT INTO authorities (user_id, authority) VALUES (3, 'ROLE_ADMIN');
INSERT INTO authorities (user_id, authority) VALUES (4, 'ROLE_USER');
INSERT INTO authorities (user_id, authority) VALUES (4, 'ROLE_ADMIN');
INSERT INTO authorities (user_id, authority) VALUES (5, 'ROLE_USER');


 